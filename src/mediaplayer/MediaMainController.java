/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mediaplayer;

import Models.HistoryModel;
import java.io.File;
import static java.lang.Math.floor;
import static java.lang.String.format;
import java.net.URL;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;
import static javafx.application.Platform.runLater;
import javafx.beans.InvalidationListener;
import javafx.beans.Observable;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.MenuItem;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.Slider;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.Tooltip;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.scene.media.MediaView;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.util.Duration;
import javax.swing.JOptionPane;
import util.ConnectionUtil;



/**
 *
 * @author CLINTON
 */
public class MediaMainController implements Initializable {
    
    
   @FXML
    private TableView<HistoryModel> table;
    @FXML
    private TableColumn<HistoryModel,String> col_id;
    @FXML
    private TableColumn<HistoryModel,String> col_name;
     @FXML
    private TableColumn<HistoryModel,String> col_url;
     @FXML
    private Button pausebtn;
      @FXML
    private Button playbtn;
       @FXML
    private Button forwardbtn;
      @FXML
      public  MediaView mediaView;
      @FXML
      public Slider slider;
      @FXML
       public Slider volume;
      @FXML
     Label time;
       @FXML
       MediaPlayer mediaPlayer;
        @FXML
       Duration duration;
        @FXML
       private Label filename;
        File selectedFile;
         @FXML
         public Pane pane;
         private Button listbtn;
        @FXML
        private ListView listview;
        @FXML 
        private MenuItem playIMenuItem,uploadMenuItem,deleteListItem,stopMenuItem,deleteHistory;
         Connection con = null;
    Statement stmt = null;
    ResultSet rs = null;
    String path;
    String date;
    String name;
    int id;
       ObservableList<HistoryModel> oblist=FXCollections.observableArrayList();
    //constructor
        public MediaMainController(){
              try{
        con = ConnectionUtil.connectdb();
       stmt=con.createStatement();
       loaddata();
        }catch(Exception n){
             System.out.println(n.getMessage());
        }
        }
        
        
    public void UploadFile(ActionEvent event) {
        try{
         FileChooser fc = new FileChooser();
         fc.getExtensionFilters().addAll(
     new FileChooser.ExtensionFilter("Files", "*.mp3","*.mp4","*.wav","*.mkv"));
       selectedFile = fc.showOpenDialog(new Stage());
       if(filename.getText().equals("No file")){
            startPlay();
       }else{
           mediaPlayer.stop();
            startPlay();
       }
        }catch(Exception n){
            JOptionPane.showMessageDialog(null,n.getMessage(),"error",JOptionPane.ERROR_MESSAGE);
        }
}  
    public void startPlay(){
        try{
         playbtn.setDisable(true);
        pausebtn.setDisable(false);
        Media media = new Media(new File(selectedFile+"").toURI().toString()); 
         mediaPlayer = new MediaPlayer(media); 
          mediaView.setMediaPlayer(mediaPlayer);
               mediaPlayer.setAutoPlay(true);
              filename.setText(selectedFile.getName());
              saveMusic();
              
              volume.valueProperty().addListener(new InvalidationListener() { 
                public void invalidated(Observable ov) 
                { 
                    if (volume.isPressed()) { 
                        mediaPlayer.setVolume(volume.getValue() / 100); // It would set the volume 
                        // as specified by user by pressing 
                    } 
                } 
            }); 
           
              
        mediaPlayer.currentTimeProperty().addListener((Observable ov) -> {
updateValues();
});
        mediaPlayer.setOnReady(() -> {
duration = mediaPlayer.getMedia().getDuration();
updateValues();
});
        
        
        slider.valueProperty().addListener(new InvalidationListener() {
public void invalidated(Observable ov) {
if (slider.isValueChanging()) {
// multiply duration by percentage calculated by slider position
if (duration != null) {
mediaPlayer.seek(duration.multiply(slider.getValue() / 100.0));
}
updateValues();

}
}
});
        }catch(Exception n){
            
        }
    }
    
    
  protected void updateValues() {
if (time != null && slider!=null) {
runLater(() -> {
Duration currentTime = mediaPlayer.getCurrentTime();
time.setText(formatTime(currentTime, duration));
slider.setDisable(duration.isUnknown());
if (!slider.isDisabled() && duration.greaterThan(Duration.ZERO) && !slider.isValueChanging()) {
slider.setValue(currentTime.divide(duration).toMillis() * 100.0);
}

});
}
} 
 
  
  private static String formatTime(Duration elapsed, Duration duration) {
int intElapsed = (int) floor(elapsed.toSeconds());
int elapsedHours = intElapsed / (60 * 60);
if (elapsedHours > 0) {
intElapsed -= elapsedHours * 60 * 60;
}
int elapsedMinutes = intElapsed / 60;
int elapsedSeconds = intElapsed - elapsedHours * 60 * 60
- elapsedMinutes * 60;

if (duration.greaterThan(Duration.ZERO)) {
int intDuration = (int) floor(duration.toSeconds());
int durationHours = intDuration / (60 * 60);
if (durationHours > 0) {
intDuration -= durationHours * 60 * 60;
}
int durationMinutes = intDuration / 60;
int durationSeconds = intDuration - durationHours * 60 * 60
- durationMinutes * 60;
if (durationHours > 0) {
return format("%d:%02d:%02d/%d:%02d:%02d",
elapsedHours, elapsedMinutes, elapsedSeconds,
durationHours, durationMinutes, durationSeconds);
} else {
return format("%02d:%02d/%02d:%02d",
elapsedMinutes, elapsedSeconds, durationMinutes,
durationSeconds);
}
} else {
if (elapsedHours > 0) {
return format("%d:%02d:%02d", elapsedHours,
elapsedMinutes, elapsedSeconds);
} else {
return format("%02d:%02d", elapsedMinutes,
elapsedSeconds);
}
}
}
  
  
 public void PauseMusic(ActionEvent event){
     try{
        mediaPlayer.pause();
        playbtn.setDisable(false);
        pausebtn.setDisable(true);
     }catch(Exception n){
        JOptionPane.showMessageDialog(null,n.getMessage(),"error",JOptionPane.ERROR_MESSAGE); 
     }
 }
 public void PlayMusic(ActionEvent event){
     try{
         mediaPlayer.play();
        playbtn.setDisable(true);
        pausebtn.setDisable(false);
     }catch(Exception n){
       JOptionPane.showMessageDialog(null,n.getMessage(),"error",JOptionPane.ERROR_MESSAGE);  
     }
 }
 public void ForwardMusic(ActionEvent event){
     try{
  mediaPlayer.seek(mediaPlayer.getCurrentTime().multiply(1.5));       
     }catch(Exception n){
        JOptionPane.showMessageDialog(null,n.getMessage(),"error",JOptionPane.ERROR_MESSAGE);
     }
 }
 public void BackwardMusic(ActionEvent event){
     try{
  mediaPlayer.seek(mediaPlayer.getCurrentTime().divide(1.5));      
     }catch(Exception n){
        JOptionPane.showMessageDialog(null,n.getMessage(),"error",JOptionPane.ERROR_MESSAGE);
     }
 }
 
 
 
 public void UploadList(ActionEvent event) {
     try{
     FileChooser fc = new FileChooser();
    fc.getExtensionFilters().addAll(
     new FileChooser.ExtensionFilter("Files", "*.mp3","*.mp4","*.wav","*.mkv"));
    List<File> selectedFiles = fc.showOpenMultipleDialog(null);
    if (selectedFiles != null) {
        for (int i = 0; i < selectedFiles.size(); i++) {
            listview.getItems().add(selectedFiles.get(i).getAbsolutePath());
        }
    } else {
        System.out.println("File is not Valid");
    }
     }catch(Exception n){
         JOptionPane.showMessageDialog(null,n.getMessage(),"error",JOptionPane.ERROR_MESSAGE);
     }
 }
 
    public void selectMusic(MouseEvent event) {
        try{
            if(event.getClickCount()==2){
            selectedFile=new File(listview.getSelectionModel().getSelectedItem()+"");
            if(filename.getText().equals("No file")){
            startPlay();
       }else{
           mediaPlayer.stop();
            startPlay();
       }
            }
        }catch(Exception n){
          JOptionPane.showMessageDialog(null,n.getMessage(),"error",JOptionPane.ERROR_MESSAGE);  
        }
    }
    
     public void stopmusic(ActionEvent event) {
         try{
             mediaPlayer.stop();
         }catch(Exception n){
             System.out.println(n.getMessage());
         }
     }
     public void RemoveItem(ActionEvent event) {
         try{
             final int selectedIdx = listview.getSelectionModel().getSelectedIndex();
             listview.getItems().remove(selectedIdx);
         }catch(Exception n){
             JOptionPane.showMessageDialog(null,n.getMessage(),"error",JOptionPane.ERROR_MESSAGE);  
         }
     }
     
     public void saveMusic(){
         try{
             name=selectedFile.getName();
             
             String check="SELECT name FROM history where name='"+name+"'";
             rs=stmt.executeQuery(check);
             if(rs.next()){
               //do nothing if foud 
             }
             else{
                   Date dated=new Date();
            DateFormat ft=new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
             date=ft.format(dated);//insert this date
            String url=selectedFile.getAbsolutePath().replace('\\', '/');
             String sql="INSERT INTO history VALUES('"+id+"','"+name+"','"+url+"','"+date+"')";
             stmt.executeUpdate(sql);
              checkdata();
             loaddata();
             }         
         }catch(Exception n){
             System.out.println(n.getMessage());
         }
     }
     
     public void loaddata(){
           try{
        String sql="SELECT * FROM history ORDER BY id desc";
        rs=stmt.executeQuery(sql);
        while(rs.next()){
            oblist.add(new HistoryModel(rs.getString("id"),rs.getString("name"),rs.getString("url").replace('/', '\\')));
        }
        }catch(Exception n){
            System.out.println(n.getMessage());
        }
     }
     
     public void checkdata(){
      for ( int i = 0; i<table.getItems().size(); i++) {
    table.getItems().clear();
}
    }
     
     public void PlayFromtable(MouseEvent event){
         if (event.getClickCount() > 1) {
      HistoryModel row = table.getSelectionModel().getSelectedItem();
        selectedFile=new File(row.getUrl());
        if(filename.getText().equals("No file")){
            startPlay();
       }else{
           mediaPlayer.stop();
            startPlay();
       }
    }
    }
     
       public void deleteHistory(ActionEvent event) {
           try{
              HistoryModel row = table.getSelectionModel().getSelectedItem();
        String file=row.getId();
        String sql="DELETE FROM history WHERE id='"+file+"'";
        stmt.executeUpdate(sql);
         checkdata();
        loaddata();
           }catch(Exception n){
               
           }
       }
    @Override
    public void initialize(URL url, ResourceBundle rb) {
       listview.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
       final Tooltip tooltip = new Tooltip();
       tooltip.setText("Double click to play");
       listview.setTooltip(tooltip);
       
        col_id.setCellValueFactory(new PropertyValueFactory<>("id"));
        col_name.setCellValueFactory(new PropertyValueFactory<>("name"));
        col_url.setCellValueFactory(new PropertyValueFactory<>("url"));
        table.setItems(oblist);
       
       
       
    }    
    
}
