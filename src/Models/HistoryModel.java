/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models;

/**
 *
 * @author CLINTON
 */
public class HistoryModel {
    String id,name,url;
    public HistoryModel(String id,String name,String url){
          this.id=id;
          this.name=name;
          this.url=url;
    }
     public String getId(){
        return id;
    }
    public void setId(String id){
        this.id=id;
    }
    
    public String getName(){
        return name;
    }
    public void setName(String name){
        this.name=name;
    }
    
    public String getUrl(){
        return url;
    }
    public void setUrl(String url){
        this.url=url;
    }
    
}
